import QtQuick 2.1

MouseArea {
    id: menu
    /*default*/ property list<QtObject> elementsProp;
    // Work around QTBUG-29179
    default property alias elements: menu.elementsProp;
    property point pos;
    signal done();
    property string title;

    acceptedButtons: Qt.AllButtons
    onClicked: { done();}
    onWheel: { /* Swallow wheel events */}
    Component.onDestruction: { /* Prevent a warning with anchors */ parent = null;}

    // Use arrow cursor
    hoverEnabled: true
    cursorShape: Qt.ArrowCursor

    function popupSubMenu(item, menu) {
        var subMenu = item;
        var up = menu
        return function() {
            up.visible = false;
            subMenu.parent = up.parent;
            subMenu.visible = true;
            subMenu.popup();
        };
    }

    function popup() {
        background.opacity = 0.8
        menu.clicked.connect(menu.done)
        var x = pos.x
        var y = pos.y
        var extraItems = [];
        var i = 0;
        var item;
        while (i < elements.length || extraItems.length > 0) {
            item = (extraItems.length > 0) ? extraItems.shift() : elements[i++];
            if (!item)
                break;
            if (item.count) {
                for (var j = 0; j < item.count; ++j)
                    extraItems.push(item.children[j]);
                continue;
            }

            item.parent = menu;
            item.x = x;
            item.y = y;
            if (item.triggered)
                item.triggered.connect(menu.done)
            if (item.title) {
                var menuEntry = Qt.createComponent("MenuItem.qml");
                if (menuEntry.status === Component.Ready) {
                    var object = menuEntry.createObject(menu, {"x": x, "y": y});
                    if (object === null) {
                        // Error Handling
                        console.log("Error creating object");
                    }

                    // Popup in the same spot as upper menu
                    item.pos = pos
                    object.text = item.title + " >"
                    object.triggered.connect(popupSubMenu(item, menu));
                    item.done.connect(done)
                } else {
                    // Error Handling
                    console.log("Error loading component:", menuEntry.errorString()," ;  status:", menuEntry.status);
                }
                y += object.height
                continue;
            }
            y += item.height;
        }
    }

    anchors.fill: parent
    z: 2

    Rectangle {
        id: background
        color: "black"
        anchors.fill: parent
        opacity: 0.0
        Behavior on opacity {
            NumberAnimation {
                easing.type: Easing.InOutQuad
                duration: 250
            }
        }
    }
}
