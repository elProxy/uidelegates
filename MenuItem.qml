import QtQuick 2.1

Rectangle {
    id: menuItem
    property string text;
    signal triggered();
    property bool enabled: true;

    color: "transparent"
    width: 120
    height: 25
    z: 3

    Text {
        font.pixelSize: 20;
        anchors.centerIn: parent
        color: menuItem.enabled? "white" : "grey"
        text: menuItem.text
    }
    MouseArea {
        anchors.fill: parent
        onClicked: menuItem.triggered();
        // Use arrow cursor
        hoverEnabled: true
        cursorShape: Qt.ArrowCursor
    }



}
