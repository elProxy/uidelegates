TARGETPATH = QtWebEngine/UIDelegates

QML_FILES = \
    Menu.qml \
    MenuItem.qml \
    MenuSeparator.qml

OTHER_FILES = $$QML_FILES

load(qml_module)
